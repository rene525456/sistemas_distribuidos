import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

public class HolaFechaImplementacion extends UnicastRemoteObject implements IHolaFecha{

    public HolaFechaImplementacion()throws RemoteException{
        super();
    }

    public String sayHello() throws RemoteException{
        return "kaixo René";
    }

    public Date getDate() throws RemoteException{
        return new Date();
    }

    public static void main (String [] args){
        //System.setSecurityManager(new RMISecurityManager());
        try {
            System.out.println("Se levantó el servidor"); 
            IHolaFecha objeto = new HolaFechaImplementacion();
            Naming.rebind("rmi://localhost/HelloServer", objeto);
            
        } catch (Exception e) {
            System.out.println("HelloImpl exception: " + e.getMessage()); 
	        e.printStackTrace(); 
        }
    }
}