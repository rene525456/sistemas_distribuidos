import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ImplementacionRmi extends UnicastRemoteObject implements InterfazRmi{
    
    public ImplementacionRmi() throws RemoteException{
        super();
    }

    public String saludar(String nombre) throws RemoteException{
        return "Hola " + nombre;
    }
}