import java.rmi.Naming;
import java.util.Scanner;

public class Cliente{
    public static void main(String [] args){
        try {
            
            
            InterfazRmi interfaz = (InterfazRmi) Naming.lookup("rmi://localhost/oyente");
            System.out.println("Hola, ¿cómo te llamas?");
            Scanner escaner = new Scanner(System.in);
            System.out.println("Servidor: " + interfaz.saludar(escaner.next()));
            
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}