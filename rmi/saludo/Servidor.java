import java.rmi.Naming;

public class Servidor{
    public Servidor(){
        try {
            System.out.println("Estamos en el servidor");
            InterfazRmi servidor = new ImplementacionRmi();
            Naming.rebind("rmi://localhost/oyente", servidor);
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

    public static void main(String [] args){
        new Servidor();
    }
}