import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List

public interface IServidor extends Remote{
    public int autenticar(String nombre) throws RemoteException;
    public int agregar(String nombre, int sesion) throws RemoteException;
    public void enviar(String nombre, int sesionDe, int sesionA) throws RemoteException;
    public List<Mensaje> recibir(int session) throws RemoteException;
}