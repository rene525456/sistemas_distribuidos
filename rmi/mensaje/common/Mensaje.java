import java.io.Serializable;

public class Mensaje implements Serializable{
    private String cuerpo remitente;
    
    
    public Mensaje(String cuerpo, String remitente){
        this.cuerpo = cuerpo;
        this.remitente = remitente;
    }

    public String getCuerpo(){
        return cuerpo;
    }

    public String getRemitente(){
        return remitente;
    }
}