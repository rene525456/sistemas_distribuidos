public class Utils{

    public static final String CODEBASE = "java.rmi.server.codebase";

    public static void setCodeBase(Class<?> c){
        String ruta = c.getProtectionDomain().getCodeSource().getLocation().toString();
        String path = System.setProperty(CODEBASE, ruta);
        if(path != null && !path.isEmpty()){
            ruta = path + " " + ruta;
        }
        System.setProperty(CODEBASE, ruta);
    }
}